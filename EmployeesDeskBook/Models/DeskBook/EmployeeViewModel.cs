﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace EmployeesDeskBook.Models.Default
{
    public class EmployeeViewModel
    {   
        public int Id { get; set; }

        [Display(Name = "Имя")]
        [Required(AllowEmptyStrings = false, ErrorMessage = " Поле обязательно для ввода")]
        [StringLength(20, ErrorMessage = "Превышено максимальное колличество вводимых символов ")]
        public string Name { get; set; }

        [Display(Name = "Фамилия")]
        [Required(AllowEmptyStrings = false, ErrorMessage = " Поле обязательно для ввода")]
        [StringLength(20, ErrorMessage = "Превышено максимальное колличество вводимых символов ")]
        public string Sername { get; set; }

        [Display(Name = "Возраст")]
        [Required(AllowEmptyStrings = false, ErrorMessage = " Поле обязательно для ввода")]
        [Range(1,30, ErrorMessage = "Введен не верный возраст")]
        public int Age { get; set; }

        [Display(Name = "Отдел")]
        [StringLength(20, ErrorMessage = "Превышено максимальное колличество вводимых символов ")]
        public string Departament { get; set; }

        [Display(Name = "Дата рождения")]
        [DataType(DataType.Date, ErrorMessage = "Введен не верный формат даты")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yy}")]
        public DateTime BirthDate { get; set; }
    }
}