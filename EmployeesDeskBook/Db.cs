﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EmployeesDeskBook.Models.Default;

namespace EmployeesDeskBook
{
    public static class Db
    {
        static List<EmployeeViewModel>_list = null;

        static Db()
        {
            _list = Enumerable.Range(1, 5).Select((x, y) => new EmployeeViewModel
            {
                Id = x,
                Age = 5 * x,
                Departament = x.ToString() + 1,
                Name = "Employee" + x,
                Sername = "Employee" + x,
                BirthDate = DateTime.Now
            }).ToList();
            ;
        }

        public static List<EmployeeViewModel> getList()
        {
            return _list;
        }

        public static void AddEmployee(EmployeeViewModel employee)
        {
            _list.Add(employee);
        }

        public static void EditEmployee(EmployeeViewModel employee)
        {

         _list.Where(x => x.Id == employee.Id).ToList().ForEach(p =>
            {
                p.Name = employee.Name;
                p.Sername = employee.Sername;
                p.Age = employee.Age;
                p.Departament = employee.Departament;
                p.BirthDate = employee.BirthDate;
            });
            
        }
    }
}