﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EmployeesDeskBook.Models.Default;


namespace EmployeesDeskBook.Controllers
{
    public class DeskBookController : Controller
    {
        private List<EmployeeViewModel> _list = null;

        public DeskBookController()
        {
            _list = Db.getList();
        }


        public ActionResult Index(int? id)
        {
            return View(new IndexViewModel
            {
                Id = id 
            });
        }

        [HttpGet]
        public ActionResult Employees(int? id)
        {

            if(id.HasValue)
            {
                return View("Employee", _list.FirstOrDefault(x => x.Id == id));
            }
            else
            {
                return View(_list);
            }
    
        }
        
        [HttpGet]
        public ActionResult CreateEmployee()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateEmployee(EmployeeViewModel employee)
        {
           
            if (!ModelState.IsValid)
            {
                return View("CreateEmployee", employee);
            }
            employee.Id = _list.Count + 1;
            Db.AddEmployee(employee);
            return RedirectToAction("Employees");
        }

        [HttpGet]
        public ActionResult EditEmployee(int id)
        {
            return View(_list.FirstOrDefault(x => x.Id == id));
        }

        [HttpPost]
        public ActionResult EditEmployee(EmployeeViewModel employee)
        {
            if(!ModelState.IsValid)
            {
                return View("EditEmployee", employee);
            }
            Db.EditEmployee(employee);
            return RedirectToAction("Employees");
        }
    }
}